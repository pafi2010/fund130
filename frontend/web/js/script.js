$(document).ready(function () {
    //submenu
    if ($(window).width() < '700') {
        $('.header__menu-item').click(function () {
            $(this).children('.submenu').slideToggle();
        });
    } else if ($(window).width() < '900') {
        $('.header__menu-item').click(function () {
            $('.submenu').slideUp();
            $(this).children('.submenu').slideToggle()
        });
    } else {
        $('.header__menu-item').hover(function () {
            $(this).toggleClass('hover');
            $(this).children('.submenu').stop(false, true).slideToggle()
        });
    }

    //mobile menu
    $('.header__menu-btn').click(function () {
        $(this).toggleClass('active');
        $('.header__menu').slideToggle();
        $(document).mouseup(function (e) {
            var div = $(".header__menu");
            var btn = $(".header__menu-btn");
            if (!div.is(e.target)
                && div.has(e.target).length === 0
                && !btn.is(e.target)
                && btn.has(e.target).length === 0) {
                $('.header__menu').slideUp();
                $('.header__menu-btn').removeClass('active');
            }
        })
    });

    //trustees/government more
    $('.trustees__more-btn').click(function () {
        $(this).siblings('.trustees__more').slideToggle();
        $(this).siblings('.trustees__image').toggleClass('active');
        $(this).toggleClass('active');
        if ($(this).hasClass('active')) {
            $(this).text('Свернуть')
        } else {
            $(this).text('Хочу знать больше')
        }
    });

    $('.government__more-btn').click(function () {
        $(this).siblings('.government__more').slideToggle();
        $(this).siblings('.government__image').toggleClass('active');
        $(this).toggleClass('active');
        if ($(this).hasClass('active')) {
            $(this).text('Свернуть')
        } else {
            $(this).text('Хочу знать больше')
        }
    })
});