function bindPhoneMask(selector) {
    $(selector).inputmask({
        'mask': '+7 (999) 999-99-99',
        'removeMaskOnSubmit': false,
        'autoUnmask': true
    });
}
