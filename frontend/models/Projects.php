<?php
namespace frontend\models;

class Projects extends \common\models\extended\Projects {
    public static function getAll($type = false, $mainId = false) {
        $query = self::find();
        if($type) {
            $query->where(['type' => $type]);
        }
        $projects = $query->orderBy('title')->all();
        $result = [
            'main' => null,
            'others' => [],
        ];
        if(!empty($projects)) {
            if($mainId) {
                foreach($projects as $index => $project) {
                    /** @var $project self */
                    if($project->id == $mainId) {
                        $result['main'] = $project;
                        unset($projects[$index]);
                        break;
                    }
                }
            }
            if(!$result['main']) {
                $result['main'] = $projects[0];
                unset($projects[0]);
            }
        }
        $result['others'] = $projects;

        return $result;
    }
}
