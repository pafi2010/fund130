<?php
namespace frontend\models;

use common\components\Notifier\NotifierTrait;
use common\components\Notifier\Notifyable;

class Admins extends \common\models\extended\Admins implements Notifyable {
    use NotifierTrait;

    public static function notifyTemplates() {
        return [
            'newQuestion' => [
                'mail_template' => 'adminNewQuestion',
                'mail_subject' => 'Вопрос с сайта фонда лицея №130',
                'sms_template' => 'Вопрос с сайта фонда лицея №130',
            ],
        ];
    }
}
