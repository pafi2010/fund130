<?php

namespace frontend\models;

use frontend\models\forms\DonationForm;

class Donations extends \common\models\extended\Donations
{
    public static function donate(DonationForm $donationForm)
    {
        $donation = new self;
        $donation->first_name = $donationForm->firstName;
        $donation->last_name = $donationForm->lastName;
        $donation->sum = $donationForm->sum;
        $donation->email = $donationForm->email;
        $donation->phone = $donationForm->phone;
        $donation->is_monthly = $donationForm->isMonthly;
        $donation->payment_purpose_id = $donationForm->purpose;
        $donation->is_completed = '0';
        if ($donation->save()) {
            return $donation;
        }
/*        echo '<pre>';
        print_r($donation);
        print_r($donation->errors);
        echo '</pre>';*/

        return false;
    }
}
