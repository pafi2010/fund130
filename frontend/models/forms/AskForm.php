<?php
namespace frontend\models\forms;

use yii\base\Model;

class AskForm extends Model {
    public $email;
    public $subject;
    public $body;

    public function rules() {
        return [
            [['email', 'body'], 'required'],
            ['email', 'email'],
            ['subject', 'string'],
            ['body', 'safe'],
        ];
    }
}
