<?php
namespace frontend\models\forms;

use yii\base\Model;

class DonationForm extends Model {
    public $firstName;
    public $lastName;
    public $phone;
    public $email;
    public $sum;
    public $isMonthly;
    public $purpose;

    public function rules() {
        return [
            ['email', 'required', 'message' => 'Укажите ваш E-mail'],
            ['sum', 'required', 'message' => 'Укажите сумму пожертвования'],
            ['purpose', 'required', 'message' => 'Укажите назначение платежа'],
            ['email', 'email'],
            [['firstName', 'lastName', 'phone'], 'string'],
            [['sum', 'purpose'], 'integer'],
            ['isMonthly', 'boolean'],
        ];
    }
}
