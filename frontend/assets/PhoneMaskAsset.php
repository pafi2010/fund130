<?php
namespace frontend\assets;

use yii\web\AssetBundle;

class PhoneMaskAsset extends AssetBundle {
    public $sourcePath = '@frontend/web/js';
    public $css = [];
    public $js = [
        'phoneMask.js'
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\widgets\MaskedInputAsset',
    ];
}
