<?php
namespace frontend\assets;

use yii\web\AssetBundle;

class DonationAsset extends AssetBundle {
    public $sourcePath = '@frontend/web/js';
    public $css = [];
    public $js = [
        'donation.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'frontend\assets\PhoneMaskAsset',
    ];
}