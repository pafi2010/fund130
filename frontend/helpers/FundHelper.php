<?php
namespace frontend\helpers;

use yii\helpers\Html;

class FundHelper {
    public static function makePageTitle($breadcrumbs) {
        array_shift($breadcrumbs);
        $title = !empty($breadcrumbs) ? implode(' - ', array_reverse($breadcrumbs)) : 'Главная';
        return Html::encode($title . " - Фонд развития лицея №130");
    }
}