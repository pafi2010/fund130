<?php
use common\helpers\SKHelper;
use frontend\models\News;

/** @var $news News[] */
?>
<section class="activity">
    <div class="wrapper wrapper_activity">
        <div class="activity__container">
            <?php foreach($data as $date => $news):?>
                <div>
                <p class="activity__date"><?=SKHelper::rusDate($date)?></p>
                    <?php foreach($news as $new):?>
                        <!--<pre><?/*= print_r($new); die(); */?></pre>-->
                        <a class="activity__item" href="<?=$new->makeFileHref()?>" target="_blank">
                            <p class="activity__name"><?=$new->title?> <?=date('d.m.Y', strtotime($new->published_at))?></p>
                        </a>
                    <?php endforeach;?>
                </div>
            <?php endforeach;?>
        </div>
    </div>
</section>
