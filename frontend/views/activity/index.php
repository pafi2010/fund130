<?php
use common\helpers\SKHelper;
use frontend\models\News;

/** @var $news News[] */
?>
<section class="activity">
    <div class="wrapper wrapper_activity">
        <div class="activity__container">
            <div>
                <?php foreach($types as $type => $caption):?>
                    <a class="activity__link" href="/activity/<?=$type?>"><?=$caption?></a>
                <?php endforeach;?>
            </div>
        </div>
    </div>
</section>
