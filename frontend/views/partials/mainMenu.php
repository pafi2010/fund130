<?php
use frontend\models\Projects;
use frontend\models\Peoples;
use frontend\models\News;
?>
<nav class="header__menu">
    <ul>
        <li class="header__menu-item">
            <a href="/about">О Фонде</a>
            <ul class="submenu">
                <li class="submenu__item"><a href="/about/structure">Структура управления</a></li>
                <?php foreach(Peoples::types() as $type => $caption):?>
                    <li class="submenu__item"><a href="/about/<?=$type?>"><?=$caption?></a></li>
                <?php endforeach;?>
                <li class="submenu__item"><a href="/about/docs">Документы (Устав и др.)</a></li>
                <li class="submenu__item"><a href="/about/requisites">Реквизиты</a></li>
                <li class="submenu__item"><a href="/about/contacts">Контактная информация</a></li>
            </ul>
        </li>
        <li class="header__menu-item">
            <a href="/projects">Проекты</a>
            <ul class="submenu">
                <?php foreach(Projects::types() as $type => $caption):?>
                    <li class="submenu__item"><a href="/projects/<?=$type?>"><?=$caption?></a></li>
                <?php endforeach;?>
            </ul>
        </li>
        <li class="header__menu-item">
            <a href="/activity">Текущая деятельность</a>
            <ul class="submenu">
                <?php foreach(News::types() as $type => $caption):?>
                    <li class="submenu__item"><a href="/activity/<?=$type?>"><?=$caption?></a></li>
                <?php endforeach;?>
            </ul>
        </li>
        <li class="header__menu-item"><a href="/partners">Наши партнеры</a></li>
        <li class="header__menu-item"><a href="/ask">Задать вопрос</a></li>
    </ul>
    <a class="header__button-mob" type="button" href="/donation">Хочу помочь!</a>
</nav>