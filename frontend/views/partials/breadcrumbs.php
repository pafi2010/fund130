<?php
$breadcrumbs = $this->params['breadcrumbs'];
?>
<?php if(count($breadcrumbs) > 1):?>
    <ul class="breadcrumbs<?=(Yii::$app->request->url == '/' ? ' breadcrumbs_white' : '')?> wrapper">
        <?php $i = 1?>
        <?php foreach($breadcrumbs as $url => $caption):?>
            <li class="breadcrumbs__item"><a<?=($i < count($breadcrumbs) ? " href='$url'" : '')?>><?=$caption?></a></li>
            <?php $i++;?>
        <?php endforeach;?>
    </ul>
<?php endif;?>
