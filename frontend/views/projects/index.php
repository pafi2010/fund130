<?php
use common\components\ImageManager\ImageManager;
use common\helpers\SKHelper;
use frontend\models\Projects;

/** @var $projects array */
?>
<section class="projects">
    <div class="wrapper wrapper_projects">

        <div class="projects__main">
            <?php if($projects['main']):?>
                <?php
                /** @var Projects $project */
                $project = $projects['main'];
                ?>
                <div class="projects__image">
                    <img src="<?=ImageManager::getUrl($project, 'image', 360, 250, ImageManager::RESIZE_ZOOM_IN)?>" alt="">
                </div>
                <p class="projects__name"><?=$project->title?></p>
                <p class="projects__description">
                    <?=$project->description?>
                </p>
                <?php if($project->type == \common\models\extended\Projects::TYPE_ACTUAL):?>
                    <a class="projects__link" href="/donation?purpose=<?=$project->payment_purpose_id?>">
                        Сделать взнос на этот проект
                    </a>
                <?php endif;?>
            <?php else:?>
                Нет проектов в выбранной категории
            <?php endif;?>
        </div>

        <?php if(!empty($projects['others'])):?>
            <div class="projects__bar">
                <p class="projects__bar-title">Другие проекты:</p>
                <?php foreach($projects['others'] as $project):?>
                    <?php /** @var Projects $project */?>
                    <div class="projects__item">
                        <div class="projects__image">
                            <img src="<?=ImageManager::getUrl($project, 'image', 440, 215, ImageManager::RESIZE_ZOOM_IN)?>" alt="">
                        </div>
                        <a class="projects__name" href="/projects/<?=$project->type?>/<?=$project->id?>"><?=$project->title?></a>
                        <p class="projects__description"><?=SKHelper::smartCut($project->description)?></p>
                        <a class="projects__more" href="/projects/<?=$project->type?>/<?=$project->id?>">Подробно</a>
                    </div>
                <?php endforeach;?>
            </div>
        <?php endif;?>
    </div>
</section>
