<?php
use common\components\ImageManager\ImageManager;
use frontend\models\Partners;

/** @var Partners[] $partners */
?>
<section class="partners">
    <div class="wrapper wrapper_partners">
        <?php foreach($partners as $partner):?>
            <a class="partners__item" href="<?=$partner->url?>" target="_blank">
                <div class="partners__image">
                    <img src="<?=ImageManager::getUrl($partner, 'logo', 235, 235, ImageManager::RESIZE_ZOOM_IN)?>" alt="" style="width: 70%">
                </div>
                <p class="partners__name"><?=$partner->title?></p>
            </a>
        <?php endforeach;?>
    </div>
</section>
