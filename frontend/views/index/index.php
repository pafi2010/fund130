<?php

use common\components\ImageManager\ImageManager;

$this->title = 'Фонд развития лицея №130';
?>
<section class="main">
    <div class="main__banner"></div>
    <div class="wrapper wrapper_main">
        <div class="main__title">Актуальные проекты:</div>
        <?php foreach($actualProjects as $project):?>
            <div class="main__item">
                <div class="main__image">
                    <img src="<?=ImageManager::getUrl($project, 'image', 360, 250, ImageManager::RESIZE_ZOOM_IN)?>" alt="">
                </div>
                <p class="main__name" href="javascript:void(0)"><?=$project->title?></p>
                <p class="main__description"><?=$project->description?></p>
                <a class="main__more" href="/projects/<?=$project->type?>/<?=$project->id?>">Подробно</a>
            </div>
        <?php endforeach;?>
        <div class="main__title">Реализованные проекты:</div>
        <?php foreach($executedProjects as $project):?>
            <div class="main__item">
                <div class="main__image">
                    <img src="<?=ImageManager::getUrl($project, 'image', 360, 250, ImageManager::RESIZE_ZOOM_IN)?>" alt="">
                </div>
                <p class="main__name" href="javascript:void(0)"><?=$project->title?></p>
                <p class="main__description"><?=$project->description?></p>
                <a class="main__more" href="/projects/<?=$project->type?>/<?=$project->id?>">Подробно</a>
            </div>
        <?php endforeach;?>
    </div>
</section>
