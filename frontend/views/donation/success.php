<section class="help">
    <div class="wrapper wrapper_help">
        <?php if(Yii::$app->session->hasFlash('success')):?>
            <div class="success"><?=Yii::$app->session->getFlash('success')?></div>
        <?php endif;?>
        <p class="help__description">Спасибо, что вы заглянули на эту страницу! Добрые дела начинаются с малого: с однажды протянутой руки помощи, с конкретного адресата, с первого шага, такого трудного и такого важного.</p>
    </div>
</section>
