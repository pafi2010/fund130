<section class="help">
    <div class="wrapper wrapper_help">
        <?php if(Yii::$app->session->hasFlash('success')):?>
            <div class="success"><?=Yii::$app->session->getFlash('success')?></div>
        <?php endif;?>

        <?php if(Yii::$app->session->hasFlash('error')):?>
            <div class="error"><?=Yii::$app->session->getFlash('error')?></div>
        <?php endif;?>
    </div>
</section>
