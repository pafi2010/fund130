<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\assets\DonationAsset;

/* @var $donationForm \frontend\models\forms\DonationForm */
?>
<section class="help">
    <div class="wrapper wrapper_help">
        <?php if(Yii::$app->session->hasFlash('success')):?>
            <div class="success"><?=Yii::$app->session->getFlash('success')?></div>
        <?php endif;?>
        <p class="help__description">Спасибо, что вы заглянули на эту страницу! Добрые дела начинаются с малого: с однажды протянутой руки помощи, с конкретного адресата, с первого шага, такого трудного и такого важного.</p>
        <p class="help__description">Если вам нравится то, что мы делаем, поддержите нас финансово, сделав пожертвование на любую сумму! Перевод осуществляется через Расчетный счет фонда Местный городской общественный фонд «Фонд развития лицея № 130» (МГОФ «Фонд-130»)</p>
        <?php $form = ActiveForm::begin([
            'options' => ['class' => 'help__form'],
            'fieldConfig' => [
                'template' => "{input}{error}",
                'errorOptions' => ['tag' => 'p', 'class' => 'error'],
            ]
        ]);?>
            <div class="help__input-wrap">
                <?=$form->field($donationForm, 'firstName')
                    ->textInput(['maxlength' => true, 'class' => 'help__input', 'placeholder' => 'Ваше имя:'])?>
            </div>
            <div class="help__input-wrap">
                <?=$form->field($donationForm, 'lastName')
                    ->textInput(['maxlength' => true, 'class' => 'help__input', 'placeholder' => 'Фамилия:'])?>
            </div>
            <div class="help__input-wrap">
                <?=$form->field($donationForm, 'phone')
                    ->textInput(['maxlength' => true, 'class' => 'help__input', 'placeholder' => 'Номер телефона:'])?>
            </div>
            <div class="help__input-wrap">
                <?=$form->field($donationForm, 'email')
                    ->textInput(['maxlength' => true, 'class' => 'help__input', 'placeholder' => 'E-mail адрес (обязательно):'])?>
            </div>
            <div class="help__input-wrap">
                <?=$form->field($donationForm, 'purpose')
                    ->dropDownList(['' => 'Выберите назначение платежа (обязательно):'] + $paymentsPurposes, ['class' => 'help__input', 'error' => 'Email cannot be blank', 'options'=>[ $_GET['purpose'] ?? '' => ["selected"=>true]]]);?>
            </div>
            <div class="help__input-wrap">
                <?=$form->field($donationForm, 'sum')
                    ->textInput(['maxlength' => true, 'class' => 'help__input', 'placeholder' => 'Сумма (обязательно):', 'type' => 'number'])?>
            </div>

            <div class="help__checkbox">
                <?=$form->field($donationForm, 'isMonthly', ['template' => '{input}{label}'])
                    ->checkbox(['class' => 'help__checkbox-input'], false)->label('Помогать ежемесячно')?>
            </div>
            <p class="help__description monthly">Если вы поставите эту галочку, пожертвования будут производиться с вашей карты автоматически каждый месяц. Вы можете остановить регулярные платежи в любой момент.</p>
            <?=Html::submitButton('Пожертвовать', ['class' => 'help__send'])?>
        <?php ActiveForm::end();?>
    </div>
</section>
<?php
DonationAsset::register($this);
?>
