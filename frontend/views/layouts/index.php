<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\AppAsset;
use frontend\helpers\FundHelper;

AppAsset::register($this);
?>
<?php $this->beginPage()?>
<!DOCTYPE html>
<html lang="<?=Yii::$app->language?>">
    <head>
        <meta charset="<?=Yii::$app->charset?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="format-detection" content="telephone=no">
        <title><?=FundHelper::makePageTitle($this->params['breadcrumbs'])?></title>
        <?=Html::csrfMetaTags()?>
        <?php $this->head()?>
    </head>
    <body>
        <?php $this->beginBody()?>
        <header class="header">
            <div class="wrapper wrapper_header">
                <a class="header__logo" href="/"></a>
                <a class="header__button" href="/donation" type="button">Хочу помочь!</a>
                <button class="header__menu-btn">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
            </div>
            <?=$this->render('../partials/mainMenu')?>
            <?=$this->render('../partials/breadcrumbs')?>
        </header>
        <?=$content?>
        <footer class="footer">
            <div class="wrapper wrapper_footer">
                <div class="footer__about">
                    <a class="footer__logo" href="/"></a>
                    <?php
                    $currentYear = date('Y');
                    $beginYear = 2016;
                    ?>
                    <a class="footer__copy" href="/">© <?=$beginYear?><?=($currentYear > $beginYear ? "-$currentYear" : '')?>. Официальный сайт Фонда-130</a>
                </div>

                <div class="footer__contact">
                    <p class="footer__contact-item"><span>Контакты:</span> <?=Yii::$app->params['globalSettings']['address']?></p>
                    <p class="footer__contact-item"><span>Контактный телефон:</span> <a href="tel:+7(383)<?=Yii::$app->params['globalSettings']['phone']?>"><?=Yii::$app->params['globalSettings']['phone']?></a></p>
                </div>

                <div class="footer__social">
                    <?php if(!empty(Yii::$app->params['globalSettings']['lyceumUrl'])):?>
                        <a class="footer__to-site" href="<?=Yii::$app->params['globalSettings']['lyceumUrl']?>" target="_blank">Сайт лицея №130</a>
                    <?php endif;?>
                    <?php foreach(['twitter', 'fb', 'vk'] as $social):?>
                        <?php if(!empty(Yii::$app->params['globalSettings'][$social])):?>
                            <a class="footer__social-item footer__social-item-<?=$social?>" href="<?=Yii::$app->params['globalSettings'][$social]?>" target="_blank"></a>
                        <?php endif;?>
                    <?php endforeach;?>
                </div>
            </div>
        </footer>
        <?php $this->endBody()?>
    </body>
</html>
<?php $this->endPage()?>
