<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $askForm \frontend\models\forms\AskForm */
?>
<section class="question">
    <div class="wrapper wrapper_question">
        <p class="question__description">Eсли Вы хотитезадать вопрос, или внести предложение для рассмотрения Правлением Фонда, пожалуйста, воспользуйтесь этой формой:</p>
        <?php if(Yii::$app->session->hasFlash('success')):?>
            <div class="success"><?=Yii::$app->session->getFlash('success')?></div>
        <?php endif;?>
        <?php $form = ActiveForm::begin([
                'options' => ['class' => 'question__form'],
                'fieldConfig' => [
                    'template' => "{input}{error}",
                    'errorOptions' => ['tag' => 'p', 'class' => 'error'],
                ]
        ]);?>
            <div class="question__input-wrap">
                <?=$form->field($askForm, 'email')
                    ->textInput(['maxlength' => true, 'class' => 'question__input important-input', 'placeholder' => 'Ваш E-mail *(обязательное поле)'])?>
            </div>
            <div class="question__input-wrap">
                <?=$form->field($askForm, 'subject')
                    ->textInput(['maxlength' => true, 'class' => 'question__input', 'placeholder' => 'Тема обращения'])?>
            </div>
            <div class="question__textarea-wrap">
                <?=$form->field($askForm, 'body')
                    ->textarea(['class' => 'question__textarea important-input', 'placeholder' => 'Текст обращения *(обязательное поле)'])?>
            </div>
            <?=Html::submitButton('Отправить', ['class' => 'question__send'])?>
        <?php ActiveForm::end();?>
    </div>
</section>
