<?php
use frontend\models\Docs;

/** @var Docs[] $docs */
$columnsCount = 3;
$countInColumn = ceil(count($docs) / $columnsCount);
$column1 = array_slice($docs, 0, $countInColumn);
$column2 = array_slice($docs, $countInColumn, $countInColumn);
$column2 = array_slice($docs, $countInColumn * 2);
?>

<section class="docs">
    <div class="wrapper wrapper_docs">
        <?php for($i = 0; $i < $columnsCount; $i++):?>
        <div class="docs__column">
            <?php foreach(array_slice($docs, $countInColumn * $i, $countInColumn) as $doc):?>
                <a class="docs__item" href="<?=$doc->makeFileHref()?>" target="_blank"><?=$doc->title?></a>
            <?php endforeach;?>
        </div>
        <?php endfor;?>
    </div>
</section>