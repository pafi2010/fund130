<section class="requisites">
    <div class="wrapper wrapper_requisites">
        <div class="requisites__block">
            <p class="requisites__title">Реквизиты</p>
            <p class="requisites__paragraph"><span>Дата основания Фонда:</span> <?=Yii::$app->params['globalSettings']['foundationAt']?></p>
            <p class="requisites__paragraph"><span>Юридический адрес:</span> <?=strip_tags(Yii::$app->params['globalSettings']['address'])?></p>
            <p class="requisites__paragraph"><span>ОКПО:</span> <?=Yii::$app->params['globalSettings']['okpo']?></p>
            <p class="requisites__paragraph"><span>ОКВЭД:</span> <?=Yii::$app->params['globalSettings']['okved']?></p>
            <p class="requisites__paragraph"><span>ОКАТО:</span> <?=Yii::$app->params['globalSettings']['okato']?></p>
            <p class="requisites__paragraph"><span>Директор:</span> <?=Yii::$app->params['globalSettings']['director']?></p>
            <p class="requisites__paragraph"><span>Главный бухгалтер:</span> <?=Yii::$app->params['globalSettings']['accountant']?></p>
            <p class="requisites__paragraph"><span>Email:</span> <a href="mailto:<?=Yii::$app->params['globalSettings']['accountantEmail']?>"><?=Yii::$app->params['globalSettings']['accountantEmail']?></a></p>
            <p class="requisites__paragraph"><span>Тел.:</span> <a href="tel:<?=Yii::$app->params['globalSettings']['accountantPhone']?>"><?=Yii::$app->params['globalSettings']['accountantPhone']?></a></p>
        </div>

        <div class="requisites__block">
            <p class="requisites__title">Банковские реквизиты</p>
            <p class="requisites__paragraph"><span>Получатель:</span> <?=Yii::$app->params['globalSettings']['recipient']?></p>
            <p class="requisites__paragraph"><span>ИНН / КПП:</span> <?=Yii::$app->params['globalSettings']['inn']?> / <?=Yii::$app->params['globalSettings']['kpp']?></p>
            <p class="requisites__paragraph"><span class="sber-label"></span><span><?=Yii::$app->params['globalSettings']['bankName']?></span> <?=Yii::$app->params['globalSettings']['bankCity']?></p>
            <p class="requisites__paragraph"><span>БИК:</span> <?=Yii::$app->params['globalSettings']['bik']?></p>
            <p class="requisites__paragraph"><span>Расч.счет:</span> № <?=Yii::$app->params['globalSettings']['account']?></p>
            <p class="requisites__paragraph"><span>Корр.счет:</span> № <?=Yii::$app->params['globalSettings']['corrAccount']?></p>
            <p class="requisites__paragraph"><span>В назначении платежа просим указать:</span> <br>«Благотворительный взнос на __________» (указать назначение по своему желанию: на систему безопасности лицея, на летние ремонты или иное. Можно ничего не писать, кроме «Благотворительный взнос» - это обязательно должно быть указано!)</p>
        </div>
    </div>
</section>
