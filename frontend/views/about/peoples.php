<?php
use common\components\ImageManager\ImageManager;
use common\helpers\SKHelper;
use frontend\models\Peoples;

/** @var Peoples[] $peoples */
/** @var string $type */
$cssClass = ($type == Peoples::TYPE_DIRECTION ? 'government' : 'trustees');
?>
<section class="<?=$cssClass?>">
    <div class="wrapper wrapper_<?=$cssClass?>">
        <?php if(!empty($peoples)):?>
            <?php foreach($peoples as $people):?>
                <div class="<?=$cssClass?>__item">
                    <div class="<?=$cssClass?>__image">
                        <img src="<?=ImageManager::getUrl($people, 'photo', 260, 260, ImageManager::RESIZE_ZOOM_IN)?>" alt="">
                    </div>
                    <p class="<?=$cssClass?>__name"><?=$people->name?></p>
                    <p class="<?=$cssClass?>__about"><?=nl2br(SKHelper::smartCut($people->description))?></p>
                    <div class="<?=$cssClass?>__more">
                        <p class="<?=$cssClass?>__about"><?=nl2br($people->description)?></p>
                    </div>
                    <button class="<?=$cssClass?>__more-btn" type="button">Хочу знать больше</button>
                </div>
            <?php endforeach;?>
        <?php endif;?>
    </div>
</section>



