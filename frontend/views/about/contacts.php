<section class="contact">
    <div class="wrapper wrapper_contact">
        <div class="contact__paragraph">
            <p class="contact__title">Контакты:</p>
            <p><?=Yii::$app->params['globalSettings']['address']?></p>
        </div>

        <div class="contact__paragraph">
            <p class="contact__title">Контактный телефон:</p>
            <p>Секретарь Лицея и Фонда-130</p>
            <p><?=Yii::$app->params['globalSettings']['secretary']?></p>
            <p>Тел.: <a href="tel:+7(383)<?=Yii::$app->params['globalSettings']['phone']?>"><?=Yii::$app->params['globalSettings']['phone']?></a></p>
        </div>
    </div>
</section>