<section class="structure">
    <div class="wrapper wrapper_structure">
        <p class="structure__title">Структура самоуправления родительского сообщества Лицея №130</p>
        <div class="structure__image">
            <img src="/img/structure.jpg" alt="">
            <img src="/img/structure-mob.jpg" alt="">
        </div>
    </div>
</section>
