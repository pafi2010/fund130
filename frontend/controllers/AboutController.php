<?php
namespace frontend\controllers;

use frontend\models\Docs;
use yii\web\NotFoundHttpException;
use frontend\models\Peoples;

class AboutController extends AppController {
    public function beforeAction($action) {
        if (!parent::beforeAction($action)) {
            return false;
        }
        $this->view->params['breadcrumbs']['/about'] = 'О фонде';
        return true;
    }

    public function actionIndex() {
        return $this->redirect(['requisites']);
    }

    public function actionStructure() {
        $this->view->params['breadcrumbs'][] = 'Структура управления';
        return $this->render('structure');
    }

    public function actionRequisites() {
        $this->view->params['breadcrumbs'][] = 'Реквизиты';
        return $this->render('requisites');
    }

    public function actionContacts() {
        $this->view->params['breadcrumbs'][] = 'Контактная информация';
        return $this->render('contacts');
    }

    public function actionPeoples($type = false) {
        if(!$type || !isset(Peoples::types()[$type])) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $this->view->params['breadcrumbs']["/about/$type"] = Peoples::types()[$type];
        return $this->render('peoples', [
            'peoples' => Peoples::find()->where(['type' => $type])->all(),
            'type' => $type,
        ]);
    }

    public function actionDocs() {
        $this->view->params['breadcrumbs']["/about/docs"] = 'Документы';
        return $this->render('docs', [
            'docs' => Docs::find()->all(),
        ]);
    }
}
