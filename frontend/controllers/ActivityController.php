<?php

namespace frontend\controllers;

use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use frontend\models\News;

class ActivityController extends AppController
{
    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return false;
        }
        $this->view->params['breadcrumbs']['/activity'] = 'Текущая деятельность';

        return true;
    }

    public function actionIndex($type = false)
    {
        if ($type) {
            $news = News::find();
            if (!isset(News::types()[$type])) {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
            $this->view->params['breadcrumbs']['/activity/' . $type] = News::types()[$type];
            $news->where(['type' => $type]);

            return $this->render('type', [
                'data' => ArrayHelper::index($news->orderBy(['published_at' => SORT_DESC])->all(), null,
                    'published_at'),
            ]);
        } else {
            return $this->render('index', [
                'types' => News::types(),
            ]);
        }
    }


}
