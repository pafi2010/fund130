<?php

namespace frontend\controllers;

use frontend\models\PaymentPurpose;
use Yii;
use frontend\models\Donations;
use frontend\models\forms\DonationForm;
use YandexCheckout\Client as YandexPayments;
use yii\helpers\ArrayHelper;

class DonationController extends AppController
{
    public function beforeAction($action)
    {
        $this->view->params['breadcrumbs']['/'] = 'Главная';

        return true;
    }

    /**
     * Страница оплаты
     *
     * @return string|\yii\web\Response
     */
    public function actionIndex()
    {
        $donationForm = new DonationForm();
        $paymentsPurposes = ArrayHelper::map(PaymentPurpose::find()->all(),'id','title');

        if ($donationForm->load(Yii::$app->request->post())) {
            if ($donationForm->validate()) {
                // Делаем запись в бд
                $donation = Donations::donate($donationForm);

                if ($donation->id) {
                    // Формируем заявку на оплату
                    $payment = $this->paymentProcess($donationForm);
                    // Обновляем информацию об оплате в БД
                    $donation->kassa_id = $payment->getId();
                    $donation->save();

                    Yii::$app->session->set('donationId', $donation->id);

                    // Редиректим на страницу оплаты
                    return $this->redirect($payment->getConfirmation()->confirmationUrl);
                }
            }
        }

        return $this->render('index', [
            'donationForm' => $donationForm,
            'paymentsPurposes' => $paymentsPurposes,
        ]);
    }

    /**
     * Запрос на оплату в Яндекс Кассу
     *
     * @param $sum
     *
     * @return \YandexCheckout\Request\Payments\CreatePaymentResponse
     */
    protected function paymentProcess($donationForm)
    {
        $client = new YandexPayments();
        $client->setAuth(Yii::$app->params['yandex']['shopId'], Yii::$app->params['yandex']['secret']);

        $response = $client->createPayment([
            'amount' => [
                'value' => $donationForm->sum,
                'currency' => 'RUB',
            ],
            'description' => PaymentPurpose::findOne($donationForm->purpose)->title ?? Yii::$app->params['yandex']['description'],
            'confirmation' => [
                'type' => 'redirect',
                'return_url' => Yii::$app->params['yandex']['return_url'],
            ],
            'capture' => true,
        ], uniqid('', true));

        return $response;
    }

    /**
     * Страница результата платежа
     *
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionCheck()
    {
        $session = Yii::$app->session;
        $session->open();
        if (!$session->get('donationId')) {
            throw new \yii\web\NotFoundHttpException();
        }

        $donation = Donations::findOne($session->get('donationId'));

        if (!$donation) {
            throw new \yii\web\NotFoundHttpException();
        }

        if ($donation->is_completed) {
            $paymentStatus = 'succeeded';
        } else {
            $client = new YandexPayments();
            $client->setAuth(Yii::$app->params['yandex']['shopId'], Yii::$app->params['yandex']['secret']);
            $payment = $client->getPaymentInfo($donation->kassa_id);
            $paymentStatus = $payment->getStatus();
        }

        if ($paymentStatus === 'succeeded') {
            $donation->is_completed = '1';
            $donation->save(false);

            Yii::$app->session->setFlash('success', 'Спасибо! Ваше пожертвование успешно отправлено.');
        } elseif ($paymentStatus === 'canceled') {
            Yii::$app->session->setFlash('error', 'Ошибка! Попробуйте повторить операцию.');
        } else {
            Yii::$app->session->setFlash('error', 'Ошибка! Попробуйте повторить операцию.');
        }

        return $this->render('result');
    }

    /**
     * Метод для обратного вызова Яндекс Кассой, для оповещения о результате платежа
     *
     * @return string
     */
    public function actionConfirm()
    {
        $request = Yii::$app->request;
        $data = json_decode($request->getRawBody());
        if ($data->type === 'notification' && $data->event === 'payment.succeeded' && $data->object->status === 'succeeded' && $data->object->paid === true) {
            // Делаем запись о проведении платежа
            $donation = Donations::findOne(['kassa_id' => $data->object->id]);
            $donation->is_completed = '1';
            $donation->save(false);
        }

        return;
    }
}
