<?php
namespace frontend\controllers;

use frontend\models\Partners;

class PartnersController extends AppController {
    public function beforeAction($action) {
        if (!parent::beforeAction($action)) {
            return false;
        }
        $this->view->params['breadcrumbs']['/partners'] = 'Партнеры';
        return true;
    }

    public function actionIndex() {
        return $this->render('index', [
            'partners' => Partners::find()->all(),
        ]);
    }
}
