<?php
namespace frontend\controllers;

use frontend\models\Projects;

class IndexController extends AppController {
    public $layout = 'index';

    public function actionIndex() {
        return $this->render('index', [
            'actualProjects' => Projects::find()->where(['type' => \common\models\extended\Projects::TYPE_ACTUAL])->all(),
            'executedProjects' => Projects::find()->where(['<>', 'type', \common\models\extended\Projects::TYPE_ACTUAL])->all(),
        ]);
    }
}
