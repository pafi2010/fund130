<?php
namespace frontend\controllers;

use Yii;
use frontend\models\Admins;
use frontend\models\forms\AskForm;

class AskController extends AppController {
    public function beforeAction($action) {
        if (!parent::beforeAction($action)) {
            return false;
        }
        $this->view->params['breadcrumbs']['/activity'] = 'Задать вопрос';
        return true;
    }

    public function actionIndex() {
        $askForm = new AskForm();
        if($askForm->load(Yii::$app->request->post())) {
            if($askForm->validate()) {
                if(Admins::email('newQuestion', Yii::$app->params['adminEmail'], ['question' => $askForm])) {
                    Yii::$app->session->setFlash('success', 'Сообщение отправлено');
                    return $this->redirect(['index']);
                }
            }
        }
        return $this->render('index', [
            'askForm' => $askForm,
        ]);
    }
}
