<?php

namespace frontend\controllers;

use yii\web\NotFoundHttpException;
use frontend\models\Projects;

class ProjectsController extends AppController
{
    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return false;
        }
        $this->view->params['breadcrumbs']['/projects'] = 'Проекты';

        return true;
    }

    public function actionIndex($type = false, $id = false)
    {
        $projects = Projects::getAll($type, $id);
        if ($type) {
            if (!isset(Projects::types()[$type])) {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
            $this->view->params['breadcrumbs']['/projects/' . $type] = Projects::types()[$type];
            if ($id) {
                if (!$projects['main'] || $projects['main']->id != $id) {
                    throw new NotFoundHttpException('The requested page does not exist.');
                }
                $this->view->params['breadcrumbs']["/projects/$type/$id"] = $projects['main']->title;
            }
        }

        return $this->render('index', [
            'projects' => $projects,
        ]);
    }
}
