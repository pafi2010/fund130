<?php
namespace backend\models;

use common\components\ImageManager\ImageManager;

class Partners extends \common\models\extended\Partners {
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        ImageManager::upload($this, 'logo');
    }
}
