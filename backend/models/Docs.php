<?php
namespace backend\models;

use Yii;

class Docs extends \common\models\extended\Docs {
    public $uploadFile;

    public function rules() {
        $rules = parent::rules();
        $rules[] = [['title'], 'required'];
        $rules[] = ['uploadFile', 'required', 'when' => function($model) {
            return $model->isNewRecord;
        }, 'whenClient' => "function() {
            return {$this->isNewRecord};
        }"];
        $rules[] = [['uploadFile'], 'file', 'extensions' => 'png, jpg, doc, docx, xls, xlsx, pdf', 'maxFiles' => 1];
        return $rules;
    }

    public function attributeLabels() {
        $labels = parent::attributeLabels();
        $labels['uploadFile'] = Yii::t('app', 'Upload File');
        return $labels;
    }

    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        $this->saveUploadedFile($this->uploadFile);
    }
}
