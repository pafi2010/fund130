<?php
namespace backend\models;

use common\components\ImageManager\ImageManager;

class Peoples extends \common\models\extended\Peoples {
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        ImageManager::upload($this, 'photo');
    }
}
