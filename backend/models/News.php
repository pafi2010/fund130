<?php
namespace backend\models;

use Yii;

class News extends \common\models\extended\News {
    public $uploadFile;

    public function rules() {
        $rules = parent::rules();
        $rules[] = [['title', 'type', 'published_at'], 'required'];
        $rules[] = ['uploadFile', 'required', 'when' => function($model) {
            return $model->isNewRecord;
        }, 'whenClient' => "function() {
            return {$this->isNewRecord};
        }"];
        $rules[] = [['uploadFile'], 'file', 'extensions' => 'png, jpg, doc, docx, xls, xlsx, pdf', 'maxFiles' => 1];
        return $rules;
    }

    public function attributeLabels() {
        $labels = parent::attributeLabels();
        $labels['uploadFile'] = Yii::t('app', 'Upload File');
        return $labels;
    }

    public function beforeSave($insert) {
        if(parent::beforeSave($insert)) {
            if($this->published_at != null) {
                $this->published_at = date('Y-m-d H:i:s', strtotime($this->published_at));
            }
            return true;
        }
        return false;
    }

    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        $this->saveUploadedFile($this->uploadFile);
    }
}
