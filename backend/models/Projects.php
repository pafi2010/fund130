<?php
namespace backend\models;

use common\components\ImageManager\ImageManager;

class Projects extends \common\models\extended\Projects {
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        ImageManager::upload($this, 'image');
    }
}
