<?php
return [
    // Common
    'Login' => 'Войти',
    'Logout' => 'Выйти',
    'Settings' => 'Настройки',
    'Name' => 'ФИО',
    'Title' => 'Название',
    'Update {modelClass}: ' => 'Изменить {modelClass}: ',
    'Create {modelClass}' => 'Добавить {modelClass}',
    'Update' => 'Обновить',
    'Delete' => 'Удалить',
    'Save' => 'Сохранить',
    'Are you sure you want to delete this item?' => 'Вы действительно хотите удалить?',
    'Create' => 'Добавить',
    'Is Active' => 'Активный',
    'Value' => 'Значение',
    'Type' => 'Тип',
    'Status' => 'Статус',
    'Created At' => 'Дата создания',
    'Updated At' => 'Дата последнего изменения',
    '(not set)' => '(не задано)',
    'Remove' => 'Удалить',
    'Add' => 'Добавить',
    'Select Picture' => 'Выбрать изображение',
    'File size can\'t be more then {maxSize} KB' => 'Допустимый размер файла - не более {maxSize} KB',
    'Cancel' => 'Отмена',
    'Yes' => 'Да',
    'No' => 'Нет',
    'Description' => 'Описание',
    'Image' => 'Изображение',
    'File' => 'Файл',
    'Upload File' => 'Файл',
    'Yandex Kassa' => 'ID платежа из Яндекс.Кассы',
    'Payment Purpose' => 'Назначение платежа',

    // Admin
    'Change password' => 'Изменить пароль',
    'Admins' => 'Администраторы',
    'Current password' => 'Текущий пароль',
    'New password' => 'Новый пароль',
    'Repeat new password' => 'Повторите новый пароль',
    'Password' => 'Пароль',
    'Remember me' => 'Запомнить меня',
    'Profile' => 'Профиль',
    'Please fill out the following fields to login:' => 'Пожалуйста, заполните форму ниже для входа:',
    'Password changed successfully' => 'Пароль успешно изменен',
    'Password change failed' => 'Не удалось изменить пароль. Попробуйте еще раз',

    // Projects
    'Projects' => 'Проекты',
    'Create Projects' => 'Добавить проект',
    'Update Projects: {nameAttribute}' => 'Изменить проект "{nameAttribute}"',
    'actual' => 'Актуальные проекты',
    'environment' => 'Безопасная среда',
    'reconstruction' => 'Реконструкция лицея',
    'edu' => 'Образовательные проекты',
    'events' => 'Поддержка внутрилицейских мероприятий',

    // Peoples
    'Peoples' => 'Люди',
    'Create Peoples' => 'Добавить человека',
    'Photo' => 'Фото',
    'trustee' => 'Попечительский совет',
    'auditor' => 'Ревизионная комиссия',
    'direction' => 'Правление',
    'Update Peoples: {nameAttribute}' => 'Изменить человека "{nameAttribute}"',

    // Partners
    'Partners' => 'Партнеры',
    'Create Partners' => 'Добавить партнера',
    'Logo' => 'Логотип',
    'Update Partners: {nameAttribute}' => 'Изменить партнера "{nameAttribute}"',

    // Docs
    'Docs' => 'Документы',
    'Create Docs' => 'Создать документ',
    'Update Docs: {nameAttribute}' => 'Изменить документ: "{nameAttribute}"',

    // News
    'News' => 'Текущая деятельность',
    'Create News' => 'Добавить документ',
    'Published At' => 'Дата публикации',
    'protocol' => 'Протоколы заседаний',
    'report' => 'Отчеты о поступлении средств',
    'cost' => 'Информация о расходах',
    'Update News: {nameAttribute}' => 'Изменить документ: "{nameAttribute}"',

    // Donations
    'Donations' => 'Пожертвования',
    'First Name' => 'Имя',
    'Last Name' => 'Фамилия',
    'Email' => 'Email',
    'Phone' => 'Телефон',
    'Sum' => 'Сумма',
    'Is Monthly' => 'Ежемесячное',
    'Is Completed' => 'Транзакция завершена',

    // Purposes
    'Purposes' => 'Назначения платежей',
    'Create Purpose' => 'Добавить назначение платежа',
    'Update Purpose: {nameAttribute}' => 'Изменить назначение платежа "{nameAttribute}"',
];