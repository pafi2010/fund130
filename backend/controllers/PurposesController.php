<?php
namespace backend\controllers;

use backend\models\PaymentPurpose;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

class PurposesController extends AppController {
    public function actionIndex() {
        $searchModel = new PaymentPurpose();
        $dataProvider = new ActiveDataProvider(['query' => $searchModel::find()]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id) {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate() {
        $model = new PaymentPurpose();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id) {
        if (($model = PaymentPurpose::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionRemoveImage($id, $attribute) {
        $model = $this->findModel($id);
        $model->updateAttributes([$attribute => null]);
        return $this->redirect(Yii::$app->request->referrer);
    }
}
