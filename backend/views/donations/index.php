<?php
use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\Donations;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\DonationsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = Yii::t('app', 'Donations');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="donations-index">
    <h1><?=Html::encode($this->title)?></h1>
    <?=GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'email:email',
            'last_name',
            'sum',
            [
                'attribute' => 'is_completed',
                'format' => 'raw',
                'value' => function ($data) {
                    /** @var $data Donations */
                    return $data->is_completed ? 'Да' : 'Нет';
                },
            ],
            'created_at',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}'
            ],
        ],
    ]);?>
</div>
