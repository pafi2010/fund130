<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Donations */
$this->title = "$model->last_name $model->first_name ($model->email)";
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Donations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="donations-view">
    <h1><?=Html::encode($this->title)?></h1>
    <?=DetailView::widget([
        'model' => $model,
        'attributes' => [
            'first_name',
            'last_name',
            'email:email',
            'phone',
            'sum',
            [
                'attribute' => 'is_monthly',
                'format' => 'raw',
                'value' => $model->is_monthly ? 'Да' : 'Нет',
            ],
            [
                'attribute' => 'is_completed',
                'format' => 'raw',
                'value' => $model->is_completed ? 'Да' : 'Нет',
            ],
            'created_at',
        ],
    ])?>
</div>
