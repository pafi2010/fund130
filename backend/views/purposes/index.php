<?php
use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\Projects;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ProjectsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Projects');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="projects-index">
    <h1><?=Html::encode($this->title)?></h1>
    <p>
        <?=Html::a(Yii::t('app', 'Create Purpose'), ['create'], ['class' => 'btn btn-success'])?>
    </p>
    <?=GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'title',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);?>
</div>
