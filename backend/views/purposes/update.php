<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Projects */
$this->title = Yii::t('app', 'Update Projects: {nameAttribute}', [
    'nameAttribute' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Projects'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="projects-update">
    <h1><?=Html::encode($this->title)?></h1>
    <?=$this->render('_form', [
        'model' => $model,
    ])?>
</div>
