<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\ImageManager\ImageManager;
use backend\models\PaymentPurpose;

/* @var $this yii\web\View */
/* @var $model PaymentPurpose */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="projects-form">
    <?php $form = ActiveForm::begin();?>
    <?=$form->field($model, 'title')->textInput(['maxlength' => true])?>
    <div class="form-group">
        <?=Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success'])?>
    </div>
    <?php ActiveForm::end();?>
</div>
