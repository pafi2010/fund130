<?php
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\DocsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Docs');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="docs-index">
    <h1><?=Html::encode($this->title)?></h1>
    <p>
        <?=Html::a(Yii::t('app', 'Create Docs'), ['create'], ['class' => 'btn btn-success'])?>
    </p>
    <?=GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'title',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);?>
</div>
