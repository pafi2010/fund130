<?php
/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>
        <div class="wrap">
            <?php
            NavBar::begin([
                'brandLabel' => Yii::$app->name,
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);
            $menuItems = [];
            if (Yii::$app->user->isGuest) {
                $menuItems[] = ['label' => Yii::t('app', 'Login'), 'url' => ['/site/login']];
            } else {
                $menuItems[] = ['label' => Yii::t('app', 'Donations'), 'url' => ['/donations']];
                $menuItems[] = [
                    'label' => 'Контент',
                    'items' => [
                        ['label' => Yii::t('app', 'Projects'), 'url' => ['/projects']],
                        ['label' => Yii::t('app', 'Peoples'), 'url' => ['/peoples']],
                        ['label' => Yii::t('app', 'Partners'), 'url' => ['/partners']],
                        ['label' => Yii::t('app', 'Docs'), 'url' => ['/docs']],
                        ['label' => Yii::t('app', 'News'), 'url' => ['/news']],
                        ['label' => Yii::t('app', 'Purposes'), 'url' => ['/purposes']],
                    ]
                ];
                $menuItems[] = ['label' => Yii::t('app', 'Settings'), 'url' => ['/settings']];
                $menuItems[] = [
                    'label' => Yii::$app->user->identity->email,
                    'items' => [
                        ['label' => Yii::t('app', 'Profile'), 'url' => ['/admins/view?id=' . Yii::$app->user->identity->id]],
                        ['label' => Yii::t('app', 'Change password'), 'url' => ['/admins/change-password']],
                        '<li>'
                        . Html::beginForm(['/site/logout'], 'post')
                        . Html::submitButton(Yii::t('app', 'Logout'), ['class' => 'btn btn-link logout'])
                        . Html::endForm()
                        . '</li>',
                    ]
                ];
            }
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $menuItems,
            ]);
            NavBar::end();
            ?>

            <div class="container">
                <?=Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ])?>
                <?=Alert::widget()?>
                <?=$content?>
            </div>
        </div>
        <footer class="footer">
            <div class="container">
                <p class="pull-left">
                    <?php
                    $startYear = 2017;
                    $currentYear = date('Y');
                    ?>
                    &copy; <?=Html::encode(Yii::$app->name) ?> <?=$startYear?><?=($currentYear > $startYear ? "-$currentYear" : '')?>
                </p>
            </div>
        </footer>
        <?php $this->endBody()?>
    </body>
</html>
<?php $this->endPage()?>
