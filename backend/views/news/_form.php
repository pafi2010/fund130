<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use backend\models\News;

/* @var $this yii\web\View */
/* @var $model News */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="news-form">
    <?php $form = ActiveForm::begin()?>
    <?=$form->field($model, 'title')->textInput(['maxlength' => true])?>
    <?=$form->field($model, 'type')->dropDownList(News::types())?>
    <?=$form->field($model, 'published_at')->widget(DatePicker::className(), [
        'value' => $model->published_at,
        'language' => 'en',
        'type' => DatePicker::TYPE_INPUT,
        'readonly' => true,
        'pluginOptions' => [
            'format' => 'dd-M-yyyy',
            'endDate' => '0d',
            'autoclose' => true,
        ],
    ])?>
    <?=$form->field($model, 'uploadFile')->fileInput([])?>
    <div class="form-group">
        <?=Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success'])?>
    </div>
    <?php ActiveForm::end()?>
</div>
