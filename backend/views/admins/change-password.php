<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\forms\ChangePasswordForm */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('app','Change password');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-changepassword">
	<h1><?=Html::encode($this->title)?></h1>

	<?php $form = ActiveForm::begin([
		'id' => 'change-password-form',
		'options' => ['class' => 'form-horizontal'],
		'fieldConfig' => [
			'template' => "{label}\n
				<div class=\"col-lg-3\">{input}</div>\n
				<div class=\"col-lg-5\">{error}</div>",
			'labelOptions' => ['class' => 'col-lg-2 control-label'],
		],
	])?>
	<?=$form->field($model, 'old_password')->passwordInput()?>
	<?=$form->field($model, 'new_password')->passwordInput()?>
	<?=$form->field($model, 'repeat_new_password')->passwordInput()?>
	<div class="form-group">
		<div class="col-lg-offset-2 col-lg-11">
			<?=Html::submitButton(Yii::t('app','Change password'), [
				'class' => 'btn btn-primary',
			])?>
		</div>
	</div>
	<?php ActiveForm::end()?>
</div>