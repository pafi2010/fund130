<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Admins */

$this->title = $model->email;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admins-view">
    <h1><?=Html::encode($this->title)?></h1>
    <?=DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'is_active',
            'email:email',
        ],
    ])?>
</div>
