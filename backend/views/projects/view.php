<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use common\components\ImageManager\ImageManager;
use common\components\ImageManager\ImageManagerAsset;
use backend\models\Projects;

/* @var $this yii\web\View */
/* @var $model Projects */
$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Projects'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="projects-view">
    <h1><?=Html::encode($this->title)?></h1>
    <p>
        <?=Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary'])?>
        <?=Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ])?>
    </p>
    <?=DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'type',
                'format' => 'raw',
                'value' => Projects::types()[$model->type],
            ],
            'title',
             ImageManager::getViewAttribute($model, 'image', 200),
            'description:ntext',
        ],
    ])?>
</div>
<?php
ImageManagerAsset::register($this);
?>
