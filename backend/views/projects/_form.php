<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\ImageManager\ImageManager;
use backend\models\Projects;
use common\models\PaymentPurpose;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model Projects */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="projects-form">
    <?php $form = ActiveForm::begin();?>
    <?=$form->field($model, 'type')->dropDownList(Projects::types())?>
    <?=$form->field($model, 'title')->textInput(['maxlength' => true])?>
    <?=$form->field($model, 'payment_purpose_id')->dropDownList([0 => '']+ArrayHelper::map(PaymentPurpose::find()->all(),'id','title'))?>
    <?=ImageManager::field($form, $model, 'image')?>
    <?=$form->field($model, 'description')->textarea(['rows' => 6])?>
    <div class="form-group">
        <?=Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success'])?>
    </div>
    <?php ActiveForm::end();?>
</div>
