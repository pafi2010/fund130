<?php
use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\Peoples;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PeoplesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = Yii::t('app', 'Peoples');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="peoples-index">
    <h1><?=Html::encode($this->title)?></h1>
    <p>
        <?=Html::a(Yii::t('app', 'Create Peoples'), ['create'], ['class' => 'btn btn-success'])?>
    </p>
    <?=GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'name',
            [
                'attribute' => 'type',
                'format' => 'raw',
                'filter' => Peoples::types(),
                'value' => function ($data) {
                    /** @var $data Peoples */
                    return Peoples::types()[$data->type];
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ])?>
</div>
