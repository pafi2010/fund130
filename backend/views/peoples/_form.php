<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\Peoples;
use common\components\ImageManager\ImageManager;

/* @var $this yii\web\View */
/* @var $model Peoples */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="peoples-form">
    <?php $form = ActiveForm::begin()?>
    <?=$form->field($model, 'type')->dropDownList(Peoples::types())?>
    <?=$form->field($model, 'name')->textInput(['maxlength' => true])?>
    <?=ImageManager::field($form, $model, 'photo')?>
    <?=$form->field($model, 'description')->textarea(['rows' => 6])?>
    <div class="form-group">
        <?=Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success'])?>
    </div>
    <?php ActiveForm::end()?>
</div>
