<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Peoples */

$this->title = Yii::t('app', 'Create Peoples');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Peoples'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="peoples-create">
    <h1><?=Html::encode($this->title)?></h1>
    <?=$this->render('_form', [
        'model' => $model,
    ])?>
</div>
