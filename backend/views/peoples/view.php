<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use common\components\ImageManager\ImageManager;
use common\components\ImageManager\ImageManagerAsset;
use backend\models\Peoples;

/* @var $this yii\web\View */
/* @var $model Peoples */
$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Peoples'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="peoples-view">
    <h1><?=Html::encode($this->title)?></h1>
    <p>
        <?=Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary'])?>
        <?=Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ])?>
    </p>
    <?=DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'type',
                'format' => 'raw',
                'value' => Peoples::types()[$model->type],
            ],
            'name',
            'description:ntext',
            ImageManager::getViewAttribute($model, 'photo', 200),
        ],
    ])?>
</div>
<?php
ImageManagerAsset::register($this);
?>
