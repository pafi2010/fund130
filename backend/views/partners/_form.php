<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\ImageManager\ImageManager;

/* @var $this yii\web\View */
/* @var $model backend\models\Partners */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="partners-form">
    <?php $form = ActiveForm::begin()?>
    <?=$form->field($model, 'title')->textInput(['maxlength' => true])?>
    <?=$form->field($model, 'url')->textInput(['maxlength' => true])?>
    <?=ImageManager::field($form, $model, 'logo')?>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success'])?>
    </div>
    <?php ActiveForm::end()?>
</div>
