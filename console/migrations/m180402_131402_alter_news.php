<?php
use yii\db\Migration;

class m180402_131402_alter_news extends Migration {
    public function safeUp() {
        $this->renameColumn('{{%news}}', 'description', 'file');
        $this->alterColumn('{{%news}}', 'file', $this->string());
    }

    public function safeDown() {
        $this->alterColumn('{{%news}}', 'file', $this->text());
        $this->renameColumn('{{%news}}', 'file', 'description');
    }
}
