<?php
use yii\db\Migration;

class m180322_091653_create_projects extends Migration {
    public function safeUp() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%projects}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(63),
            'image' => $this->string(63),
            'description' => $this->text(),
        ], $tableOptions);
    }

    public function safeDown() {
        $this->dropTable('{{%projects}}');
    }
}
