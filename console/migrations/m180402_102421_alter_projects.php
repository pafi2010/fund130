<?php
use yii\db\Migration;

class m180402_102421_alter_projects extends Migration {
    public function safeUp() {
        $this->addColumn('{{%projects}}', 'type', "ENUM('actual', 'environment', 'reconstruction', 'edu', 'events') NOT NULL DEFAULT 'actual' AFTER `id`");
    }

    public function safeDown() {
        $this->dropColumn('{{%projects}}', 'type');
    }
}
