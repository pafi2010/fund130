<?php
use yii\db\Migration;

class m180322_092615_create_news extends Migration {
    public function safeUp() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%news}}', [
            'id' => $this->primaryKey(),
            'type' => "ENUM('protocol', 'report', 'cost') NOT NULL",
            'title' => $this->string(63),
            'description' => $this->text(),
            'published_at' => $this->timestamp()->null(),
        ], $tableOptions);
    }

    public function safeDown() {
        $this->dropTable('{{%news}}');
    }
}
