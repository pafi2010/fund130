<?php
use yii\db\Migration;

class m180322_093518_create_docs extends Migration {
    public function safeUp() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%docs}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(63),
            'url' => $this->string(),
        ], $tableOptions);
    }

    public function safeDown() {
        $this->dropTable('{{%docs}}');
    }
}
