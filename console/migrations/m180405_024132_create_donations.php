<?php
use yii\db\Migration;

class m180405_024132_create_donations extends Migration {
    public function safeUp() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%donations}}', [
            'id' => $this->primaryKey(),
            'is_completed' => $this->boolean()->notNull()->defaultValue(0),
            'is_monthly' => $this->boolean()->notNull()->defaultValue(0),
            'email' => $this->string()->notNull(),
            'phone' => $this->string(31)->null(),
            'sum' => $this->integer()->notNull(),
            'first_name' => $this->string(63)->null(),
            'last_name' => $this->string(63)->null(),

            'created_at' => $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP')->append('ON UPDATE CURRENT_TIMESTAMP'),
        ], $tableOptions);
    }

    public function safeDown() {
        $this->dropTable('{{%donations}}');
    }
}
