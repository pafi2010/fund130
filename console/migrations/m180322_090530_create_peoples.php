<?php
use yii\db\Migration;

class m180322_090530_create_peoples extends Migration {
    public function safeUp() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%peoples}}', [
            'id' => $this->primaryKey(),
            'type' => "ENUM('trustee', 'auditor', 'direction') NOT NULL",
            'name' => $this->string(63),
            'photo' => $this->string(63),
            'description' => $this->text(),
        ], $tableOptions);
    }

    public function safeDown() {
        $this->dropTable('{{%peoples}}');
    }
}
