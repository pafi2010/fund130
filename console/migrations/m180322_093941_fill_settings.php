<?php
use yii\db\Migration;

class m180322_093941_fill_settings extends Migration {
    public function safeUp() {
        $this->insert('{{%settings}}', ['alias' => 'vk', 'name' => 'Ссылка на группу ВКонтакте', 'value' => '']);
        $this->insert('{{%settings}}', ['alias' => 'fb', 'name' => 'Ссылка на группу в Фейсбук', 'value' => '']);
        $this->insert('{{%settings}}', ['alias' => 'twitter', 'name' => 'Ссылка на аккаунт в Твиттер', 'value' => '']);
        $this->insert('{{%settings}}', ['alias' => 'phone', 'name' => 'Телефон в контактах', 'value' => '330-35-73']);
        $this->insert('{{%settings}}', ['alias' => 'address', 'name' => 'Почтовый адрес', 'value' => '630090, Россия, город Новосибирск, улица Учёных, 10']);
        $this->insert('{{%settings}}', ['alias' => 'lyceumUrl', 'name' => 'Ссылка на сайт лицея 130', 'value' => 'http://licey130.ru/']);
        $this->insert('{{%settings}}', ['alias' => 'secretary', 'name' => 'ФИО секретаря', 'value' => 'Калачевская Лариса Игартовна']);
        $this->insert('{{%settings}}', ['alias' => 'foundationAt', 'name' => 'Дата основания фонда', 'value' => '9 июля 1999']);
        $this->insert('{{%settings}}', ['alias' => 'legalAddress', 'name' => 'Юридический адрес', 'value' => '630090, г.Новосибирск, ул. Ученых,10']);
        $this->insert('{{%settings}}', ['alias' => 'okpo', 'name' => 'ОКПО', 'value' => '51727985']);
        $this->insert('{{%settings}}', ['alias' => 'okved', 'name' => 'ОКВЭД', 'value' => '91.33']);
        $this->insert('{{%settings}}', ['alias' => 'okato', 'name' => 'ОКАТО', 'value' => '50401384000']);
        $this->insert('{{%settings}}', ['alias' => 'director', 'name' => 'ФИО директора', 'value' => 'Снегирев Сергей Васильевич']);
        $this->insert('{{%settings}}', ['alias' => 'accountant', 'name' => 'ФИО главного бухгалтера', 'value' => 'Панова Раиса Геннадьевна']);
        $this->insert('{{%settings}}', ['alias' => 'accountantEmail', 'name' => 'Email главного бухгалтера', 'value' => 'panova53@mail.ru']);
        $this->insert('{{%settings}}', ['alias' => 'accountantPhone', 'name' => 'Телефон главного бухгалтера', 'value' => '+7 913 956-54-74']);
        $this->insert('{{%settings}}', ['alias' => 'recipient', 'name' => 'Получатель платежей', 'value' => 'Местный городской общественный фонд «Фонд развития лицея № 130» (МГОФ «Фонд-130»)']);
        $this->insert('{{%settings}}', ['alias' => 'inn', 'name' => 'ИНН', 'value' => '5408161483']);
        $this->insert('{{%settings}}', ['alias' => 'kpp', 'name' => 'КПП', 'value' => '540801001']);
        $this->insert('{{%settings}}', ['alias' => 'bankName', 'name' => 'Наименование банка', 'value' => 'Сибирский банк ПАО Сбербанк']);
        $this->insert('{{%settings}}', ['alias' => 'bankCity', 'name' => 'Город банка', 'value' => 'г. Новосибирск']);
        $this->insert('{{%settings}}', ['alias' => 'bik', 'name' => 'БИК банка', 'value' => '045004641']);
        $this->insert('{{%settings}}', ['alias' => 'account', 'name' => 'Рассчетный счет', 'value' => '40 703 810 744 05 000 2202']);
        $this->insert('{{%settings}}', ['alias' => 'corrAccount', 'name' => 'Корреспондентский счет', 'value' => '30 101 810 500 00 000 0641']);
    }

    public function safeDown() {
        $this->delete('{{%settings}}', ['alias' => 'vk']);
        $this->delete('{{%settings}}', ['alias' => 'fb']);
        $this->delete('{{%settings}}', ['alias' => 'twitter']);
        $this->delete('{{%settings}}', ['alias' => 'phone']);
        $this->delete('{{%settings}}', ['alias' => 'address']);
        $this->delete('{{%settings}}', ['alias' => 'lyceumUrl']);
        $this->delete('{{%settings}}', ['alias' => 'secretary']);
        $this->delete('{{%settings}}', ['alias' => 'foundationAt']);
        $this->delete('{{%settings}}', ['alias' => 'legalAddress']);
        $this->delete('{{%settings}}', ['alias' => 'okpo']);
        $this->delete('{{%settings}}', ['alias' => 'okved']);
        $this->delete('{{%settings}}', ['alias' => 'okato']);
        $this->delete('{{%settings}}', ['alias' => 'director']);
        $this->delete('{{%settings}}', ['alias' => 'accountant']);
        $this->delete('{{%settings}}', ['alias' => 'accountantEmail']);
        $this->delete('{{%settings}}', ['alias' => 'accountantPhone']);
        $this->delete('{{%settings}}', ['alias' => 'recipient']);
        $this->delete('{{%settings}}', ['alias' => 'inn']);
        $this->delete('{{%settings}}', ['alias' => 'kpp']);
        $this->delete('{{%settings}}', ['alias' => 'bankName']);
        $this->delete('{{%settings}}', ['alias' => 'bankCity']);
        $this->delete('{{%settings}}', ['alias' => 'bik']);
        $this->delete('{{%settings}}', ['alias' => 'account']);
        $this->delete('{{%settings}}', ['alias' => 'corrAccount']);
    }
}
