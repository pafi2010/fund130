<?php
use yii\db\Migration;

class m180322_093239_create_partners extends Migration {
    public function safeUp() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%partners}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(63),
            'logo' => $this->string(63),
            'url' => $this->string(),
        ], $tableOptions);
    }

    public function safeDown() {
        $this->dropTable('{{%partners}}');
    }
}
