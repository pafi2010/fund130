<?php
use yii\db\Migration;

class m180402_125039_alter_docs extends Migration {
    public function safeUp() {
        $this->renameColumn('{{%docs}}', 'url', 'file');
    }

    public function safeDown() {
        $this->renameColumn('{{%docs}}', 'file', 'url');
    }
}
