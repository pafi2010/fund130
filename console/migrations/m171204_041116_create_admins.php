<?php
use yii\db\Migration;

class m171204_041116_create_admins extends Migration {
    public function safeUp() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%admins}}', [
            'id' => $this->primaryKey(),
            'is_active' => $this->boolean()->defaultValue(true),
            'email' => $this->string(31)->notNull()->unique(),
            'password_hash' => $this->string(),
            'auth_key' => $this->string(),
        ], $tableOptions);
    }

    public function safeDown() {
        $this->dropTable('{{%admins}}');
    }
}
