<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $question frontend\models\forms\AskForm */
?>
<div class="password-reset">
    <p>Здравствуйте, Admin!</p>
    <p>Поступил вопрос от пользователя сайта фонда лицея №130.</p>
    <p><strong>Email пользователя:</strong> <?=Html::encode($question->email)?></p>
    <p><strong>Тема вопроса:</strong> <?=Html::encode($question->subject)?></p>
    <p><strong>Вопрос:</strong> <?=Html::encode($question->body)?></p>
</div>
