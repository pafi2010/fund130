<?php
return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'yandex' => [
        'shopId' => '514804',
        'secret' => 'live_4rbNinMsIkiruD_qUGgulq3GytksnGZnd6sHBmAaaco',
        'description' => 'Добровольное пожертвование в фонд развития лицея №130',
        'return_url' => 'https://фонд-130.рф/donation/check',
    ],
];
