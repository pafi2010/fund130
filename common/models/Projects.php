<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%projects}}".
 *
 * @property int $id
 * @property string $type
 * @property string $title
 * @property string $image
 * @property string $description
 * @property int    $payment_purpose_id
 */
class Projects extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%projects}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'description'], 'string'],
            [['title', 'image'], 'string', 'max' => 63],
            [['payment_purpose_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', 'Type'),
            'title' => Yii::t('app', 'Title'),
            'image' => Yii::t('app', 'Image'),
            'description' => Yii::t('app', 'Description'),
            'payment_purpose_id' => Yii::t('app', 'Payment Purpose'),
        ];
    }
}
