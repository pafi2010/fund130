<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%donations}}".
 *
 * @property int $id
 * @property int $is_completed
 * @property int $is_monthly
 * @property string $email
 * @property string $phone
 * @property int $sum
 * @property string $first_name
 * @property string $last_name
 * @property string $created_at
 * @property string $updated_at
 * @property string $kassa_id
 * @property int $payment_purpose_id
 */
class Donations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%donations}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'sum', 'payment_purpose_id'], 'required'],
            [['sum','payment_purpose_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['is_completed', 'is_monthly'], 'string', 'max' => 1],
            [['email'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 31],
            [['first_name', 'last_name', 'kassa_id'], 'string', 'max' => 63],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'is_completed' => Yii::t('app', 'Is Completed'),
            'is_monthly' => Yii::t('app', 'Is Monthly'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Phone'),
            'sum' => Yii::t('app', 'Sum'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'kassa_id' => Yii::t('app', 'Yandex Kassa'),
            'payment_purpose_id' => Yii::t('app', 'Payment Purpose'),
        ];
    }
}
