<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%payment_purpose}}".
 *
 * @property integer $id
 * @property string $title
 */
class PaymentPurpose extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%payments_purposes}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'title' => Yii::t('app', 'Текст назначения платежа'),
        ];
    }
}
