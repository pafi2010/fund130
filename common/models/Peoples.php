<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%peoples}}".
 *
 * @property int $id
 * @property string $type
 * @property string $name
 * @property string $photo
 * @property string $description
 */
class Peoples extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%peoples}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'required'],
            [['type', 'description'], 'string'],
            [['name', 'photo'], 'string', 'max' => 63],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', 'Type'),
            'name' => Yii::t('app', 'Name'),
            'photo' => Yii::t('app', 'Photo'),
            'description' => Yii::t('app', 'Description'),
        ];
    }
}
