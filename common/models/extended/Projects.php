<?php
namespace common\models\extended;

use Yii;
use common\components\ImageManager\ImageManager;

class Projects extends \common\models\Projects {
    use AppModelTrait;

    const TYPE_ACTUAL = 'actual';
    const TYPE_ENVIRONMENT = 'environment';
    const TYPE_RECONSTRUCTION = 'reconstruction';
    const TYPE_EDU = 'edu';
    const TYPE_EVENTS = 'events';

    public $imageImage;
    public static $imageImageMaxSize = 1;

    public function rules() {
        $rules = parent::rules();
        $rules[] = ImageManager::getRule('image');
        return $rules;
    }

    public function attributeLabels() {
        $labels = parent::attributeLabels();
        $labels = ImageManager::addLabel($labels, 'image');
        return $labels;
    }

    public static function types() {
        return [
            self::TYPE_ACTUAL => Yii::t('app', self::TYPE_ACTUAL),
            self::TYPE_ENVIRONMENT => Yii::t('app', self::TYPE_ENVIRONMENT),
            self::TYPE_RECONSTRUCTION => Yii::t('app', self::TYPE_RECONSTRUCTION),
            self::TYPE_EDU => Yii::t('app', self::TYPE_EDU),
            self::TYPE_EVENTS => Yii::t('app', self::TYPE_EVENTS),
        ];
    }
}
