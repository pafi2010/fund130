<?php
namespace common\models\extended;

use Yii;

class News extends \common\models\News {
    use AppModelTrait, FileUploadTrait;

    const TYPE_PROTOCOL = 'protocol';
    const TYPE_REPORT = 'report';
    const TYPE_COST = 'cost';

    public static function types() {
        return [
            self::TYPE_REPORT => Yii::t('app', self::TYPE_REPORT),
            self::TYPE_COST => Yii::t('app', self::TYPE_COST),
            self::TYPE_PROTOCOL => Yii::t('app', self::TYPE_PROTOCOL),
        ];
    }
}
