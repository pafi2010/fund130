<?php
namespace common\models\extended;

use common\components\ImageManager\ImageManager;

class Partners extends \common\models\Partners {
    use AppModelTrait;

    public $logoImage;
    public static $logoImageMaxSize = 1;

    public function rules() {
        $rules = parent::rules();
        $rules[] = ImageManager::getRule('logo');
        $rules[] = [['url'], 'url', 'defaultScheme' => 'http'];
        return $rules;
    }

    public function attributeLabels() {
        $labels = parent::attributeLabels();
        $labels = ImageManager::addLabel($labels, 'logo');
        return $labels;
    }
}
