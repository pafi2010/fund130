<?php
namespace common\models\extended;

use Yii;
use yii\web\UploadedFile;
use common\helpers\SKHelper;

trait FileUploadTrait {
    /** @param UploadedFile|null $uploadFile */
    public function saveUploadedFile($uploadFile) {
        if($uploadFile && !$uploadFile->error) {
            $fileName = SKHelper::translit(mb_strtolower($uploadFile->name, 'utf-8'));
            $path = self::makeFilePath($fileName);
            if(!is_dir($path)) {
                mkdir($path, 0777, true);
            }
            $uploadFile->saveAs("$path/$fileName");
            $this->updateAttributes(['file' => $fileName]);
        }
    }

    public static function makeFilePath($fileName, $isHref = false) {
        $class = explode('\\', static::className());
        $class = array_pop($class);
        $first2 = substr($fileName, 0, 2);
        $first4 = substr($fileName, 0, 4);
        $host = $_SERVER['REQUEST_SCHEME'] . '://' . str_replace(['admin.', 'api.', 'uploads.'], '', $_SERVER['HTTP_HOST']) . '/uploads';
        return Yii::getAlias(($isHref ? $host : '@uploads') . "/$class/$first2/$first4");
    }

    public function makeFileA() {
        $href = $this->makeFileHref();
        return "<a href='$href' target='_blank'>{$this->file}</a>";
    }

    public function makeFileHref() {
        return self::makeFilePath($this->file, true) . "/{$this->file}";
    }
}