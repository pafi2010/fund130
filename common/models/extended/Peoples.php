<?php
namespace common\models\extended;

use Yii;
use common\components\ImageManager\ImageManager;

class Peoples extends \common\models\Peoples {
    use AppModelTrait;

    const TYPE_TRUSTEE = 'trustee';
    const TYPE_AUDITOR = 'auditor';
    const TYPE_DIRECTION = 'direction';

    public $photoImage;
    public static $photoImageMaxSize = 1;

    public function rules() {
        $rules = parent::rules();
        $rules[] = ImageManager::getRule('photo');
        return $rules;
    }

    public function attributeLabels() {
        $labels = parent::attributeLabels();
        $labels = ImageManager::addLabel($labels, 'photo');
        return $labels;
    }

    public static function types() {
        return [
            self::TYPE_DIRECTION => Yii::t('app', self::TYPE_DIRECTION),
            self::TYPE_TRUSTEE => Yii::t('app', self::TYPE_TRUSTEE),
            self::TYPE_AUDITOR => Yii::t('app', self::TYPE_AUDITOR),
        ];
    }
}
