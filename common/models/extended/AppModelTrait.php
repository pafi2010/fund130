<?php
namespace common\models\extended;

use yii\helpers\ArrayHelper;

trait AppModelTrait {
    public static function getList($keyField = 'id', $valueField = 'name', $conditions = []) {
        return ArrayHelper::map(
            self::find()->select([$keyField, $valueField])->where($conditions)->asArray()->all(),
            $keyField,
            $valueField
        );
    }

    public static function getMap($primaryField = 'id') {
        $entitiesMap = [];
        foreach (self::find()->asArray()->all() as $entity) {
            $entitiesMap[$entity[$primaryField]] = $entity;
        }
        return $entitiesMap;
    }
}