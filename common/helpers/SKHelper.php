<?php
namespace common\helpers;

class SKHelper {
    public static function smartCut($text, $length = 100) {
        $text = trim($text);
        if(mb_strlen($text, 'utf-8') > $length) {
            $end = mb_strpos($text, ' ', $length, 'utf-8');
            $end = !$end || $end > ($length * 1.1) ? $length : $end;
            return mb_substr($text, 0, $end, 'utf-8') . '...';
        } else {
            return $text;
        }
    }

    public static function ending($cnt, $endings) {
        if(($cnt / 1) > 0 && ($cnt / 1) < 1) {
            return $endings[1];
        }
        $cnt %= 100;
        if($cnt <= 10 || $cnt > 20) {
            $cnt %= 10;
            switch ($cnt) {
                case 1:
                    return $endings[0];
                case 2: case 3: case 4:
                    return $endings[1];
                default:
                    return $endings[2];
            }
        } else {
            return $endings[2];
        }
    }

    public static function translit($text) {
        $alphabet = [
            'а' => 'a',
            'А' => 'a',
            'б' => 'b',
            'Б' => 'b',
            'в' => 'v',
            'В' => 'v',
            'г' => 'g',
            'Г' => 'g',
            'д' => 'd',
            'Д' => 'd',
            'е' => 'e',
            'Е' => 'e',
            'ё' => 'e',
            'Ё' => 'e',
            'ж' => 'j',
            'Ж' => 'j',
            'з' => 'z',
            'З' => 'z',
            'и' => 'i',
            'И' => 'i',
            'й' => 'i',
            'Й' => 'i',
            'к' => 'k',
            'К' => 'k',
            'л' => 'l',
            'Л' => 'l',
            'м' => 'm',
            'М' => 'm',
            'н' => 'n',
            'Н' => 'n',
            'о' => 'o',
            'О' => 'o',
            'п' => 'p',
            'П' => 'p',
            'р' => 'r',
            'Р' => 'r',
            'с' => 's',
            'С' => 's',
            'т' => 't',
            'Т' => 't',
            'у' => 'u',
            'У' => 'u',
            'ф' => 'f',
            'Ф' => 'f',
            'х' => 'h',
            'Х' => 'h',
            'ц' => 'c',
            'Ц' => 'c',
            'ч' => 'ch',
            'Ч' => 'ch',
            'ш' => 'sh',
            'Ш' => 'sh',
            'щ' => 'shch',
            'Щ' => 'shch',
            'ъ' => '',
            'Ъ' => '',
            'ы' => 'i',
            'Ы' => 'i',
            'ь' => '',
            'Ь' => '',
            'э' => 'e',
            'Э' => 'e',
            'ю' => 'yu',
            'Ю' => 'yu',
            'я' => 'ya',
            'Я' => 'ya',
            ' ' => '_',
            '=' => '',
            '+' => '',
            '&' => '',
            '?' => '',
            '{' => '',
            '}' => '',
            '|' => '',
            '%' => '',
            '/' => '',
            '#' => '',
            ':' => '',
            '~' => '',
            '`' => '',
            '!' => '',
            '@' => '',
            '$' => '',
            '^' => '',
            '*' => '',
            '(' => '',
            ')' => '',
            '\\' => '',
            ',' => '',
            '<' => '',
            '>' => '',
            '\'' => '',
            '"' => '',
            ';' => '',
            '№' => ''
        ];
        return str_replace(array_keys($alphabet), $alphabet, $text);
    }

    public static function rusDate($date) {
        $months = [1 => 'января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];
        $date = strtotime($date);
        $month = $months[date('n', $date)];
        return date("d $month Y", $date);
    }
}