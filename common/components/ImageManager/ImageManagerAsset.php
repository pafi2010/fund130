<?php
namespace common\components\ImageManager;

use yii\web\AssetBundle;

class ImageManagerAsset extends AssetBundle {
    public $sourcePath = '@common/components/ImageManager/static';
    public $css = [
        'imageManager.css'
    ];
    public $js = [
        'imageManager.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}