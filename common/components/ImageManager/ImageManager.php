<?php
namespace common\components\ImageManager;

use Yii;
use yii\helpers\Html;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

class ImageManager {
    const DEFAULT_IMAGE_EXT = 'jpg';
    const DEFAULT_IMAGE_QUALITY = 85;

    const RESIZE_PROPORTIONAL = 0;
    const RESIZE_ZOOM_IN = 1;
    const RESIZE_ZOOM_OUT = 2;

    const RSIZE_TYPES = [
        self::RESIZE_PROPORTIONAL,
        self::RESIZE_ZOOM_IN,
        self::RESIZE_ZOOM_OUT,
    ];

    public static function getRule($attributeName, $maxSizeInMB = 5) {
        return [
            "{$attributeName}Image",
            'image',
            'extensions' => 'gif, jpg, jpeg, png',
            'maxSize' => $maxSizeInMB * 1024 * 1024,
            'skipOnEmpty' => true,
            'skipOnError' => true
        ];
    }

    public static function addLabel($labels, $attributeName) {
        $labels["{$attributeName}Image"] = $labels[$attributeName];
        return $labels;
    }

    public static function field(ActiveForm $form, $model, $attributeName) {
        /** @var $model yii\db\ActiveRecord */
        $maxSizeParam = "{$attributeName}ImageMaxSize";
        $imageUrl = self::getUrl($model, $attributeName, 200);
        return $form->field($model, "{$attributeName}Image")->widget(FileInput::classname(), [
            'options' => ['accept' => 'image/*'],
            'pluginOptions' => [
                'initialPreview' => [
                    ($imageUrl ? Html::img($imageUrl, ['style' => 'max-width: 100%; max-height: 100%']) : ""),
                ],
                'showPreview' => true,
                'layoutTemplates' => [
                    'footer' => '',
                    'actions' => '',
                ],
                'showCaption' => false,
                'showRemove' => false,
                'showUpload' => false,
                'showCancel' => false,
                'browseClass' => 'btn btn-primary btn-block',
                'browseIcon' => '<i class="glyphicon glyphicon-picture"></i> ',
                'browseLabel' => Yii::t('app', 'Select Picture'),
                'maxFileSize' => ($model::className())::$$maxSizeParam * 1024,
                'msgSizeTooLarge' => Yii::t('app', 'File size can\'t be more then {maxSize} KB'),
            ],
        ]);
    }

    public static function upload($model, $attributeName) {
        /** @var $model yii\db\ActiveRecord */
        if($image = UploadedFile::getInstance($model, "{$attributeName}Image")) {
            $fileName = self::generateFileName($image->tempName, $model->id);
            $filePath = self::touchPath($model, $attributeName, $fileName);
            self::saveAsJpg($image, "$filePath$fileName." . self::DEFAULT_IMAGE_EXT);
            $model->updateAttributes([$attributeName => $fileName]);
            return true;
        }
        return false;
    }

    private static function saveAsJpg(UploadedFile $sourceImageFile, $finalImagePath) {
        $sourceExt = explode('/', $sourceImageFile->type)[1];
        $sourceSizes = getimagesize($sourceImageFile->tempName);
        $createImageFuncName = "imagecreatefrom$sourceExt";
        $sourceFile = $createImageFuncName($sourceImageFile->tempName);
        $finalImage = imagecreatetruecolor($sourceSizes[0], $sourceSizes[1]);
        imagecopyresampled(
            $finalImage, $sourceFile,
            0, 0,
            0, 0,
            $sourceSizes[0], $sourceSizes[1],
            $sourceSizes[0], $sourceSizes[1]
        );
        imagejpeg($finalImage, $finalImagePath, self::DEFAULT_IMAGE_QUALITY);
    }

    private static function touchPath($model, $attributeName, $fileName) {
        /** @var $model yii\db\ActiveRecord */
        $path = self::makePath($model, $attributeName, $fileName);
        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }
        return $path;
    }

    private static function makePath($model, $attributeName, $fileName) {
        /** @var $model yii\db\ActiveRecord */
        $class = explode('\\', $model::className());
        $fileNameFirst2 = substr($fileName, 0, 2);
        $fileNameFirst4 = substr($fileName, 0, 4);
        return Yii::getAlias('@uploads/' . array_pop($class) . "/$attributeName/$fileNameFirst2/$fileNameFirst4/");
    }

    private static function makeUrl($model, $attributeName, $fileName) {
        /** @var $model yii\db\ActiveRecord */
        $host = 'https://' . str_replace(['admin.', 'api.', 'uploads.'], '', $_SERVER['HTTP_HOST']) . '/uploads/';
        $class = explode('\\', $model::className());
        $fileNameFirst2 = substr($fileName, 0, 2);
        $fileNameFirst4 = substr($fileName, 0, 4);
        return Yii::getAlias($host . array_pop($class) . "/$attributeName/$fileNameFirst2/$fileNameFirst4/");
    }

    private static function generateFileName($tempFileName, $modelId) {
        return md5(md5_file($tempFileName) . $modelId . time());
    }

    public static function getUrl($model, $attributeName, $width = null, $height = null, $resizeType = self::RESIZE_PROPORTIONAL) {
        /** @var $model yii\db\ActiveRecord */
        if(!empty($model->$attributeName)) {
            $url = self::makeUrl($model, $attributeName, $model->$attributeName);
            $path = self::makePath($model, $attributeName, $model->$attributeName);
            $fileName = "{$model->$attributeName}." . self::DEFAULT_IMAGE_EXT;
            $originalImagePath = "$path$fileName";
            if(file_exists($originalImagePath) && mime_content_type($originalImagePath) == 'image/jpeg') {
                if($width || $height) {
                    return $url . self::makeResizedImage($originalImagePath, $width, $height, $resizeType);
                }
                return "$url$fileName";
            }
        } return false;
    }

    public static function getViewAttribute($model, $attributeName, $width = null, $height = null, $resizeType = self::RESIZE_PROPORTIONAL) {
        $value = '<span class="not-set">' . Yii::t('app', '(not set)') . '</span>';
        if($imgUrl = self::getUrl($model, $attributeName, $width, $height, $resizeType)) {
            $removeLink = '/' . Yii::$app->controller->id . '/remove-image?id=' . $model->id . '&attribute=' . $attributeName;
            $value = "<div class='image-manager-wrapper'><img src='$imgUrl' /><a href='$removeLink'>" . Yii::t('app', 'Remove') . "</a></div>";
        }
        return [
            'attribute' => $attributeName,
            'format' => 'raw',
            'value' => $value,
        ];
    }

    private static function makeResizedImage($originalImagePath, $width, $height, $resizeType) {
        $resizeType = in_array($resizeType, self::RSIZE_TYPES) ? $resizeType : self::RESIZE_PROPORTIONAL;
        $width = $width ?: 0;
        $height = $height ?: 0;
        if($resizeType == self::RESIZE_PROPORTIONAL) {
            if($width) {
                $height = 0;
            } elseif ($height) {
                $width = 0;
            }
        }
        $originalImageName = basename($originalImagePath, '.' . self::DEFAULT_IMAGE_EXT);
        $imagePath = dirname($originalImagePath);
        $resizedFileName = "{$originalImageName}_{$width}_{$height}_{$resizeType}." . self::DEFAULT_IMAGE_EXT;
        $resizedImagePath = "$imagePath/$resizedFileName";

        if(!file_exists($resizedImagePath)) {
            $originalSizes = getimagesize($originalImagePath);
            $needleSizes = self::getNeedleImageSizes($originalSizes, $width, $height, $resizeType);
            $needleCanvasSizes = self::getCanvasChords($originalSizes, $needleSizes, $resizeType);
            $originalImage = imagecreatefromjpeg($originalImagePath);
            $resizedImage = imagecreatetruecolor($needleSizes[0], $needleSizes[1]);
            imagefill($resizedImage, 0, 0, imagecolorallocate($resizedImage, 255, 255, 255));
            imagecopyresampled(
                $resizedImage,
                $originalImage,
                $needleCanvasSizes['rw0'],
                $needleCanvasSizes['rh0'],
                $needleCanvasSizes['w0'],
                $needleCanvasSizes['h0'],
                $needleCanvasSizes['rw1'],
                $needleCanvasSizes['rh1'],
                $needleCanvasSizes['w1'],
                $needleCanvasSizes['h1']
            );
            imagejpeg($resizedImage, $resizedImagePath, self::DEFAULT_IMAGE_QUALITY);
        }
        return $resizedFileName;
    }

    private static function getNeedleImageSizes($originalSizes, $width, $height, $resizeType) {
        $resultWidth = $width;
        $resultHeight = $height;
        if($resizeType == self::RESIZE_PROPORTIONAL) {
            $leaderSide = $width ? 'width' : 'height';
            if($leaderSide == 'width') {
                $resultWidth = $width;
                $proportion = $originalSizes[0] / $resultWidth;
                $resultHeight = $originalSizes[1] / $proportion;
            } else {
                $resultHeight = $height;
                $proportion = $originalSizes[1] / $resultHeight;
                $resultWidth = $originalSizes[0] / $proportion;
            }
        }
        return [(int) $resultWidth, (int) $resultHeight];
    }

    private static function getCanvasChords($originalSizes, $needleSizes, $resizeType) {
        $w0 = $h0 = 0;
        $w1 = $originalSizes[0];
        $h1 = $originalSizes[1];
        $rw0 = $rh0 = 0;
        $rw1 = $needleSizes[0];
        $rh1 = $needleSizes[1];
        switch($resizeType) {
            case self::RESIZE_ZOOM_IN:
                $wIndex = $originalSizes[0] / $needleSizes[0];
                $hIndex = $originalSizes[1] / $needleSizes[1];
                if($wIndex > $hIndex) {
                    $originalWidthNeedle = $needleSizes[0] * $hIndex;
                    $w0 = ($originalSizes[0] - $originalWidthNeedle) / 2;
                    $w1 = $originalWidthNeedle;
                } else {
                    $originalHeightNeedle = $needleSizes[1] * $wIndex;
                    $h0 = ($originalSizes[1] - $originalHeightNeedle) / 2;
                    $h1 = $originalHeightNeedle;
                }
                break;
            case self::RESIZE_ZOOM_OUT:
                $wIndex = $originalSizes[0] / $needleSizes[0];
                $hIndex = $originalSizes[1] / $needleSizes[1];
                if($wIndex > $hIndex) {
                    $resultHeightNeedle = $originalSizes[1] / $wIndex;
                    $rh0 = ($needleSizes[1] - $resultHeightNeedle) / 2;
                    $rh1 = $resultHeightNeedle;
                } else {
                    $resultWidhtNeedle = $originalSizes[0] / $hIndex;
                    $rw0 = ($needleSizes[0] - $resultWidhtNeedle) / 2;
                    $rw1 = $resultWidhtNeedle;
                }
                break;
        }
        return [
            'w0' => (int) $w0, 'h0' => (int) $h0, 'w1' => (int) $w1, 'h1' => (int) $h1,
            'rw0' => (int) $rw0, 'rh0' => (int) $rh0, 'rw1' => (int) $rw1, 'rh1' => (int) $rh1,
        ];
    }
}
